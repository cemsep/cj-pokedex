# CjPokedex

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.24.

## Developers
Jens Hetland    @Hetland    https://gitlab.com/Hetland
Cem Pedersen    @cem        https://gitlab.com/cemsep

# Components and services
* PokemonList
    - A component and start page that displays a list of Pokemon.
* PokemonListItem
    - Child of PokemonList.
    - A component that is used to display a single pokemon in the PokemonList component.
* Searchbar 
    - Child of PokemonList.
    - A component with an EventEmitter that sends the search result to PokemonList. 
    - User must press Enter for search to take effect.
* PokemonDetail
    - A component that is used to display detailed information of a single Pokemon.
* PokemonProfileAvatar
    - Child of PokemonDetail.
    - A simple component with an Input that displays the Pokemon's image.
* PokemonProfile
    - Child of PokemonDetail.
    - A simple component with an Input that displays the Pokemon basic information:
        - Name
        - Types
        - Height
        - Weight
        - Base Experience 
* PokemonStats
    - Child of PokemonDetail.
    - A simple component with an Input to display the Pokemon’s basic stats:
        - Base Stats
        - Abilities
        - Sprites
* PokemonMoves
    - Child of PokemonDetail.
    - A component with an Input that lists all the moves the Pokemon has available.
* PokemonService
    - Service where components fetch pokemon info.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
