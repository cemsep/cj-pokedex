import { Component, OnInit } from '@angular/core';
import { Pokemon, PokemonAPI, Results } from '../pokemon';
import { PokemonService } from '../pokemon.service';


@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {

  /**
   * Global variables
   * @ignore
   */
  pokemons: PokemonAPI;
  pokemonsBackup: Results[];
  nextPokemons: PokemonAPI;
  nextUrl: string;
  searchText: string;

  constructor(
    private pokemonService: PokemonService
    ) {}

  ngOnInit() {
    this.getPokemons();
  }

  /**
   * Initial fetch of pokemons. For every pokemon fetch call the getPokemonDetails() to fetch it's details.
   * Make a backup of the response for further usage (search pokemon).
   * @ignore
   */
  getPokemons(): void {
    this.pokemonService.getPokemons().then((data: PokemonAPI) => {
      this.nextUrl = data.next;
      this.pokemons = data;
      if(data.results && data.results.length) {
        this.pokemons.results.forEach(pokemon => {
          this.getPokemonDetails(pokemon);
        });
      }
      this.pokemonsBackup = [...this.pokemons.results];
    });
  }

  /**
   * Function that calls getPokemon() inside the pokemon-service. Fetchs the details of a single pokemon.
   * @param pokemon is a Results interface which contains { name: pokemon name, url: link to next 20 pokemons }
   * @ignore
   */
  getPokemonDetails(pokemon: Results): void {
    this.pokemonService.getPokemon(pokemon.name).then((details: Pokemon) => {
      pokemon.details = details;
      pokemon.id = details.id;
    });
  }

  /**
   * Fetching the next 20 pokemons. Applying the filter if searchText exists, so that searchPokemon function keeps on working
   * for next pack of pokemons. If the searchText exists when loading more pokemons, they will also get filtered.
   * @ignore
   */
  loadMore(): void {
    this.pokemonService.loadMore(this.nextUrl).then((data: PokemonAPI) => {
      this.nextUrl = data.next;
      this.nextPokemons = data;
      this.nextPokemons.results.forEach(pokemon => {
        this.getPokemonDetails(pokemon);
      });
      if(this.searchText !== undefined && this.searchText !== '') {
        this.pokemonsBackup = this.pokemonsBackup.concat(this.nextPokemons.results);
        this.nextPokemons.results = this.nextPokemons.results.filter(pokemon => {
          return pokemon.name.includes(this.searchText);
        });
        this.pokemons.results = this.pokemons.results.concat(this.nextPokemons.results);
      } else {
        this.pokemons.results = this.pokemons.results.concat(this.nextPokemons.results);
        this.pokemonsBackup = [...this.pokemons.results];
      }
    });
  }

  /**
   * Filtering the results after searchText. To reset the search, the user only needs to type "" and press enter.
   * In that case, the pokemonsBackup that we made earlier will restore the filtered array.
   * @param searchText some search text that user types inside the searchbar.
   */
  searchPokemon(searchText) {
    if(searchText) {
      this.searchText = searchText;
      this.pokemons.results = this.pokemonsBackup;
      this.pokemons.results = this.pokemons.results.filter(pokemon => {
        return pokemon.name.includes(searchText);
      });
    } else {
      this.searchText = '';
      this.pokemons.results = this.pokemonsBackup;
    }
  }
}
