import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonProfileAvatarComponent } from './pokemon-profile-avatar.component';

describe('PokemonProfileAvatarComponent', () => {
  let component: PokemonProfileAvatarComponent;
  let fixture: ComponentFixture<PokemonProfileAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokemonProfileAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonProfileAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
