import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Pokemon, PokemonAPI } from './pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  pokemons: Pokemon[] = [];

  constructor(private http: HttpClient) { }

  /**
   * API call for getting the first 20 pokemons. Returns a promise.
   * @ignore
   */
  async getPokemons() {
    return new Promise((resolve, reject) => {
      this.http.get<PokemonAPI>('https://pokeapi.co/api/v2/pokemon/').subscribe(resolve, reject);
    });
  }

  /**
   * API call for getting a given pokemon details, uses name of the pokemon as parameter. Returns a promise.
   * @ignore
   */
  async getPokemon(name: string) {
    return new Promise((resolve, reject) => {
      this.http.get<Pokemon>(`https://pokeapi.co/api/v2/pokemon/${name}`).subscribe(resolve, reject);
    });
  }

  /**
   * API call for getting a given pokemon details, uses id of the pokemon as parameter. Returns a promise.
   * @ignore
   */
  async getPokemonById(id: number) {
    return new Promise((resolve, reject) => {
      this.http.get<Pokemon>(`https://pokeapi.co/api/v2/pokemon/${id}`).subscribe(resolve, reject);
    });
  }

  /**
   * API call for getting the next 20 pokemons. Returns a promise.
   * @ignore
   */
  async loadMore(url: string) {
    return new Promise((resolve, reject) => {
      this.http.get<PokemonAPI>(url).subscribe(resolve, reject);
    });
  }

}
