import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';

import { Pokemon } from '../pokemon';

@Component({
  selector: 'app-pokemon-profile',
  templateUrl: './pokemon-profile.component.html',
  styleUrls: ['./pokemon-profile.component.scss']
})
export class PokemonProfileComponent implements OnInit {
  @Input() pokemon: Pokemon;
  weight: number;
  height: number;

  constructor(private location: Location) { }

  ngOnInit() {
    this.weight = this.pokemon.weight;
    this.height = this.pokemon.height;

    this.weight = Math.round(this.weight * 0.1);
    this.height = Math.round(this.height * 0.1);
  }

  goBack() {
    this.location.back();
  }


}
