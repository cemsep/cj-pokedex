import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from '../pokemon';

@Component({
  selector: 'app-pokemon-stats',
  templateUrl: './pokemon-stats.component.html',
  styleUrls: ['./pokemon-stats.component.scss']
})
export class PokemonStatsComponent implements OnInit {
  @Input() pokemon: Pokemon;
  abilities = [];

  constructor() { }

  ngOnInit() {
    this.abilities = this.pokemon.abilities;
    this.abilities = this.abilities.sort((a, b) => (a.slot > b.slot) ? 1 : -1);
  }

}
