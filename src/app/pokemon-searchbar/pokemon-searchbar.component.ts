import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pokemon-searchbar',
  templateUrl: './pokemon-searchbar.component.html',
  styleUrls: ['./pokemon-searchbar.component.scss']
})
export class PokemonSearchbarComponent implements OnInit {

  @Output() searchPokemon = new EventEmitter();
  searchText: string;

  constructor() { }

  ngOnInit() {
  }

  search(e) {
    if(!e.srcElement.value) {
      this.searchPokemon.emit(null);
    } else {
      this.searchText = e.srcElement.value.toLowerCase();
      this.searchPokemon.emit(this.searchText);
    }
  }

}
