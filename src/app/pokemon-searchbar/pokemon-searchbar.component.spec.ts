import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonSearchbarComponent } from './pokemon-searchbar.component';

describe('PokemonSearchbarComponent', () => {
  let component: PokemonSearchbarComponent;
  let fixture: ComponentFixture<PokemonSearchbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokemonSearchbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonSearchbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
