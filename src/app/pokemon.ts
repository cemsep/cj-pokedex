export interface Pokemon {
  abilities: [
    {
      ability: {
        name: string;
        url;
      },
      is_hidden: true;
      slot: number;
    }
  ];
  base_experience: number;
  height: number;
  id: number;
  moves: [
    {
      move: {
        name: string;
        url: string;
      }
    }
  ];
  name: string;
  sprites: {
    back_default: string;
    back_female: string;
    back_shiny: string;
    back_shiny_female: string;
    front_default: string;
    front_female: string;
    front_shiny: string;
    front_shiny_female: string;
  };
  stats: [
    {
      base_stat: number;
      effort: number;
      stat: {
        name: string;
        url: string;
      }
    }
  ];
  types: [
    {
      slot: number;
      type: {
        name: string;
        url: string;
      }
    }
  ];
  weight: number;
}

export interface Results {
  name: string;
  url: string;
  details?: Pokemon;
  id?: number;
}

export interface PokemonAPI {
  count: number;
  next: string;
  previous: string;
  results: Results[];
}
